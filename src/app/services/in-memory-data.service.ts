import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const additional = [
      {
        id: 0,
        label: 'Extra bacon',
        price: 3,
        additionalTime: 0
      },
      {
        id: 1,
        label: 'Sem cebola',
        price: 0,
        additionalTime: 0
      },
      {
        id: 2,
        label: 'Borda recheada',
        price: 5,
        additionalTime: 5
      },
    ];
    const flavors = [
      {
        id: 0,
        label: 'Marguerita',
        additionalTime: 0,
      },
      {
        id: 1,
        label: 'Calabresa',
        additionalTime: 0,
      },
      {
        id: 2,
        label: 'Portuguesa',
        additionalTime: 5,
      }
    ];
    const sizes = [
      {
        id: 0,
        label: 'Pequena',
        price: 20,
        preparationTime: 15
      },
      {
        id: 1,
        label: 'Média',
        price: 30,
        preparationTime: 20
      },
      {
        id: 2,
        label: 'Grande',
        price: 40,
        preparationTime: 25
      }
    ];

    return { additional, flavors, sizes };
  }
}
