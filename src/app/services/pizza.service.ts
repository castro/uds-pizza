import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  private URL = 'app';

  constructor(private httpClient: HttpClient) { }

  getAdditional(): Observable<any> {
    return this.httpClient
      .get<any>(`${this.URL}/additional`)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getFlavors(): Observable<any> {
    return this.httpClient
      .get<any>(`${this.URL}/flavors`)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getSizes(): Observable<any> {
    return this.httpClient
      .get<any>(`${this.URL}/sizes`)
      .pipe(map(data => data), catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse | any): any {
    console.error(res.error || res.body.error);
    return res.error;
  }

}
