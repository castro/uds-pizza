import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingComponent } from './loading.component';
import { MatProgressBarModule, MatProgressSpinnerModule } from '@angular/material';
import { LoadingService } from './loading.service';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LoadingComponent', () => {
  let component: LoadingComponent;
  let fixture: ComponentFixture<LoadingComponent>;
  let mockLoadingService;

  beforeEach(async(() => {
    mockLoadingService = jasmine.createSpyObj(['getSubject']);
    mockLoadingService.getSubject.and.returnValue(of({}));

    TestBed.configureTestingModule({
      declarations: [
        LoadingComponent
      ],
      imports: [
        MatProgressBarModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: LoadingService, useValue: mockLoadingService }
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
