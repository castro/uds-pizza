import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class LoadingService {
  subject = new BehaviorSubject(false);

  constructor() { }

  enable(): void {
    this.subject.next(true);
  }

  disable(): void {
    this.subject.next(false);
  }

  isEnabled() {
    return this.subject.value;
  }

  getSubject(): Observable<any> {
    return this.subject.asObservable();
  }
}
