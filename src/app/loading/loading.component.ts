import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { LoadingService } from './loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  animations: [
    trigger('visibleLoading', [
      state('void', style({
        opacity: 0
      })),
      state('*', style({
        opacity: 1
      })),
      transition(':enter', animate('275ms ease-out')),
      transition(':leave', animate('275ms ease-in'))
    ])
  ]
})
export class LoadingComponent {
  constructor(public loadingService: LoadingService) { }
}
