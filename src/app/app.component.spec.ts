import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatCheckboxModule, MatProgressSpinnerModule, MatRadioModule, MatStepperModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { LoadingComponent } from './loading/loading.component';
import { LoadingService } from './loading/loading.service';
import { PizzaService } from './services/pizza.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let mockLoadingService, mockPizzaService;

  beforeEach(async(() => {
    mockLoadingService = jasmine.createSpyObj('LoadingService', ['enable', 'disable', 'getSubject']);
    mockLoadingService.getSubject.and.returnValue(of());

    mockPizzaService = jasmine.createSpyObj('PizzaService', ['getAdditional', 'getFlavors', 'getSizes']);
    mockPizzaService.getAdditional.and.returnValue(of([]));
    mockPizzaService.getFlavors.and.returnValue(of([]));
    mockPizzaService.getSizes.and.returnValue(of([]));

    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatRadioModule,
        MatCheckboxModule,
        MatCardModule,
        MatProgressSpinnerModule
      ],
      declarations: [
        AppComponent,
        LoadingComponent
      ],
      providers: [
        { provide: LoadingService, useValue: mockLoadingService },
        { provide: PizzaService, useValue: mockPizzaService }
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  describe('Whent init component', () => {
    it('should load data #getAdditional from PizzaService', () => {
      expect(mockPizzaService.getAdditional).toHaveBeenCalledTimes(1);
    });

    it('should load data #getFlavors from PizzaService', () => {
      expect(mockPizzaService.getFlavors).toHaveBeenCalledTimes(1);
    });

    it('should load data #getSizes from PizzaService', () => {
      expect(mockPizzaService.getSizes).toHaveBeenCalledTimes(1);
    });
  });
});
