import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { LoadingService } from './loading/loading.service';
import { PizzaService } from './services/pizza.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  pizzaFlavorOptions;
  pizzaSizeOptions;
  pizzaAdditionalOptions;

  preparationTime = 0;
  value = 0;

  constructor(
    private formBuilder: FormBuilder,
    private pizzaService: PizzaService,
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      size: [null, Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      flavor: [null, Validators.required]
    });
    this.thirdFormGroup = this.formBuilder.group({
      additional: [[], Validators.nullValidator]
    });
    this.loadData();
  }

  checkedBox(pAdditional, event): void {
    const index = this.thirdFormGroup.controls.additional.value.lastIndexOf(pAdditional);
    if (event.checked) {
      if (index === -1) {
        this.thirdFormGroup.controls.additional.value.push(pAdditional);
        this.pizzaUpdate();
      }
    } else {
      if (index > -1) {
        this.thirdFormGroup.controls.additional.value.splice(index, 1);
        this.pizzaUpdate();
      }
    }
  }

  flavorChange(): void {
    this.preparationTime = this.firstFormGroup.controls.size.value.preparationTime;
    this.thirdFormGroup.controls.additional.value.map(el => this.preparationTime += el.additionalTime);
  }

  loadData(): void {
    this.loadingService.enable();
    forkJoin(
      this.pizzaService.getAdditional(),
      this.pizzaService.getFlavors(),
      this.pizzaService.getSizes()
    ).pipe(take(1))
      .subscribe(([additional, flavors, sizes]) => {
        this.loadingService.disable();
        this.pizzaAdditionalOptions = additional;
        // tslint:disable-next-line:no-string-literal
        this.pizzaAdditionalOptions.map(el => el['checked'] = false);
        this.pizzaFlavorOptions = flavors;
        this.pizzaSizeOptions = sizes;
      });
  }

  pizzaUpdate(): void {
    this.value = this.firstFormGroup.controls.size.value.price;
    this.thirdFormGroup.controls.additional.value.map(el => this.value += el.price);

    this.preparationTime = this.firstFormGroup.controls.size.value.preparationTime;
    this.thirdFormGroup.controls.additional.value.map(el => this.preparationTime += el.additionalTime);
  }

  reset(): void {
    this.thirdFormGroup.controls.additional.setValue([]);
    this.pizzaAdditionalOptions.map(pAdditional => {
      // tslint:disable-next-line:no-string-literal
      pAdditional['checked'] = false;
    });
  }
}
